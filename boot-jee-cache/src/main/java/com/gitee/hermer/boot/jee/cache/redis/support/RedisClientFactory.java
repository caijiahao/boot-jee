package com.gitee.hermer.boot.jee.cache.redis.support;


import java.io.Closeable;

import com.gitee.hermer.boot.jee.cache.redis.client.RedisClient;


public interface RedisClientFactory<C extends RedisClient> extends Closeable {

    void build();

    
    C getResource();

    void returnResource(C client);

}
