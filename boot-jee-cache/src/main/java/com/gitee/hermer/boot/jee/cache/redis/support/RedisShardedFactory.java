package com.gitee.hermer.boot.jee.cache.redis.support;

import redis.clients.jedis.JedisShardInfo;
import redis.clients.jedis.ShardedJedisPool;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import com.gitee.hermer.boot.jee.cache.redis.client.ShardedRedisClient;


public class RedisShardedFactory implements RedisClientFactory<ShardedRedisClient> {

    private static ShardedJedisPool jedisPool;
    private RedisPoolConfig poolConfig;

    private List<JedisShardInfo> jedisShardInfoList;

    public synchronized ShardedJedisPool getJedisPool() {
        return jedisPool;
    }

    @Override
    public ShardedRedisClient getResource() {
        return new ShardedRedisClient(getJedisPool().getResource());
    }

    @Override
    public void returnResource(ShardedRedisClient client) {
        if (client != null)
            client.close();
    }

    @Override
	public void build() {
        
        
        String host = this.poolConfig.getHost();
        int timeout = this.poolConfig.getTimeout();
        if (host != null) {
            List<String> list = Arrays.asList(host.split(","));
            jedisShardInfoList = new ArrayList<>();
            for (String uri : list) {
                JedisShardInfo jedisShardInfo = new JedisShardInfo(uri);
                jedisShardInfo.setConnectionTimeout(timeout);
                jedisShardInfoList.add(jedisShardInfo);
            }
        }

        jedisPool = new ShardedJedisPool(poolConfig, jedisShardInfoList);
    }

    public RedisPoolConfig getPoolConfig() {
        return poolConfig;
    }

    public void setPoolConfig(RedisPoolConfig poolConfig) {
        this.poolConfig = poolConfig;
    }

    public List<JedisShardInfo> getJedisShardInfoList() {
        return jedisShardInfoList;
    }

    public void setJedisShardInfoList(List<JedisShardInfo> jedisShardInfoList) {
        this.jedisShardInfoList = jedisShardInfoList;
    }

    
    @Override
    public void close() throws IOException {
        jedisPool.close();
    }
}
