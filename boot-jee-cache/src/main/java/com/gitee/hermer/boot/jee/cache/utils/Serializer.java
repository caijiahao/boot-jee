package com.gitee.hermer.boot.jee.cache.utils;

import java.io.IOException;


public interface Serializer {
	
	public String name();

	public byte[] serialize(Object obj) throws IOException ;
	
	public Object deserialize(byte[] bytes) throws IOException ;
	
}
