package com.gitee.hermer.boot.jee.cache;

import static org.junit.Assert.assertTrue;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import com.gitee.hermer.boot.jee.cache.lock.DistributedLock;
import com.gitee.hermer.boot.jee.cache.lock.DistributedLockBuilder;
import com.gitee.hermer.boot.jee.cache.redis.RedisCacheService;
import com.gitee.hermer.boot.jee.commons.Id.generator.IdWorker;
import com.gitee.hermer.boot.jee.commons.thread.SyncFinisher;
import com.gitee.hermer.boot.jee.commons.thread.SyncFinisher.WorkInterface;

@RunWith(SpringJUnit4ClassRunner.class)
public class RedisCacheServiceTest {

	private String ID = "A";

	@Test
	public void testRedisCache() throws Exception{

		try{
			RedisCacheService.save(ID, "", null);
		}catch (Exception e) {
		}
		assertTrue(RedisCacheService.size(ID) == 0);

		SyncFinisher finisher = new SyncFinisher();

		final IdWorker idWorker = new IdWorker();
		for (int i = 0; i < 200; i++) {
			finisher.addWorker(new WorkInterface() {

				@Override
				public void work() {
					try {
						RedisCacheService.save(ID,idWorker.nextId() , ID);
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
			});
		}
		finisher.start();
		finisher.await();



		assertTrue(RedisCacheService.size(ID) == 200);

	}



	@Before
	public void clearCache(){
		RedisCacheService.clear(ID);
	} 

//	@Test
	public void testRedisCacheLock() throws Exception{


		try{

			final IdWorker idWorker = new IdWorker();

			DistributedLock lock = DistributedLockBuilder.builder(ID,8000l);
			new Thread(new Runnable() {

				@Override
				public void run() {
					DistributedLock lock = DistributedLockBuilder.builder(ID,8000l);
					lock.lock();
				}
			}).start();
			Thread.sleep(1000);
			assertTrue(DistributedLockBuilder.builder(ID,8000l).isLock());

			lock.unLock();
		}catch (Exception e) {
		}




	}

}
