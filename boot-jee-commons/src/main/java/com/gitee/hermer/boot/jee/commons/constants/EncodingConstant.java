package com.gitee.hermer.boot.jee.commons.constants;

public class EncodingConstant {

	public final static String GBK 			= "GBK";
	
	public final static String UTF8			= "UTF-8";
	
	public final static String ISO88591 	= "ISO8859-1";

}
