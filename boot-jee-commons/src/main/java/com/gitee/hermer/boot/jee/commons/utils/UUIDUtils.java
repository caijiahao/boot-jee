
package com.gitee.hermer.boot.jee.commons.utils;

import java.util.UUID;


public class UUIDUtils {
	public static String uuid(){
		String uuid =  UUID.randomUUID().toString();
		return uuid.replace("-", "");
	}
}
