package com.gitee.hermer.boot.jee.cache;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.connection.Message;
import org.springframework.data.redis.connection.MessageListener;
import org.springframework.data.redis.core.RedisTemplate;

import com.gitee.hermer.boot.jee.cache.Command.Operation;
import com.gitee.hermer.boot.jee.cache.exception.CacheException;
import com.gitee.hermer.boot.jee.cache.properties.BootCacheProperties;
import com.gitee.hermer.boot.jee.commons.collection.CollectionUtils;
import com.gitee.hermer.boot.jee.commons.log.UtilsContext;

public class BootCacheChannel extends UtilsContext implements CacheChannel,MessageListener,CacheListener{

	@Autowired
	private CacheManager manager;

	@Autowired
	private BootCacheProperties properties;

	@Autowired
	private RedisTemplate template;


	@SuppressWarnings("unchecked")
	@Override
	public <T> T get(String region, Object key) {
		try{
			Object value = manager.get(LEVEL_1, region, key);
			if(value == null){
				value = manager.get(LEVEL_2, region, key);
				manager.set(LEVEL_1, region, key, value);
			}
			return (T) value;
		}catch (Exception e) {
			error(e.getMessage(),e);
		}
		return null;
	}

	@Override
	public void set(String region, Object key, Object value) {
		try {
			if(region != null && key != null){

				if(value == null){
					evict(region, key);
				}else{
					manager.set(LEVEL_1, region, key, value);
					manager.set(LEVEL_2, region, key, value);
					sendChannelEvictCommand(region,key.toString());
				}
			}
		} catch (CacheException e) {
			error(e.getMessage(),e);
		}
	}

	@Override
	public void set(String region, Object key, Object value, Integer expireInSec) {
		try {
			if(region != null && key != null){

				if(value == null){
					evict(region, key);
				}else{
					manager.set(LEVEL_1, region, key, value,expireInSec);
					manager.set(LEVEL_2, region, key, value,expireInSec);
					sendChannelEvictCommand(region,key.toString());
				}
			}

		} catch (CacheException e) {
			error(e.getMessage(),e);
		}

	}

	@Override
	public void evict(String region, Object key) {
		try {
			manager.evict(LEVEL_1, region, key);
			manager.evict(LEVEL_2, region, key);
			sendChannelEvictCommand(region, key.toString());
		} catch (CacheException e) {
			error(e.getMessage(),e);
		}

	}

	private void sendChannelEvictCommand(String region,List keys){
		for (Object key : keys) {
			sendChannelEvictCommand(region,key.toString());
		}
	}

	private void sendChannelEvictCommand(String region,String key){
		Command command = new Command();
		command.setOperation(Operation.onDelete);
		command.setKey(key);
		command.setRegion(region);
		template.convertAndSend(properties.getRedisChannelName(),command);
	}

	private void sendChannelClearCommand(String region){
		Command command = new Command();
		command.setOperation(Operation.onClear);
		command.setRegion(region);
		template.convertAndSend(properties.getRedisChannelName(),command);
	}

	@Override
	public void batchEvict(String region, List keys) {
		try {
			manager.batchEvict(LEVEL_1, region, keys);
			manager.batchEvict(LEVEL_2, region, keys);
			sendChannelEvictCommand(region, keys);
		} catch (CacheException e) {
			error(e.getMessage(),e);
		}
	}

	@Override
	public void clear(String region) {
		try {
			manager.clear(LEVEL_1, region);
			manager.clear(LEVEL_2, region);
			sendChannelClearCommand(region);
		} catch (CacheException e) {
			error(e.getMessage(),e);
		}

	}

	@Override
	public List keys(String region){
		try {
			List l1_list = manager.keys(LEVEL_1, region);
			if(CollectionUtils.isEmpty(l1_list))
				return manager.keys(LEVEL_2, region);
			return l1_list;
		} catch (CacheException e) {
			error(e.getMessage(),e);
		}
		return null;
	}

	@Override
	public void close() {
		try{
			manager.shutdown(LEVEL_1);
			manager.shutdown(LEVEL_2);
		}catch (Exception e) {
			error(e.getMessage(),e);
		}
	}

	@Override
	public void onMessage(Message message, byte[] pattern) {
		try{
			byte[] body = message.getBody();
			Command command = (Command) template.getValueSerializer().deserialize(body);  

			if(command != null){
				switch (command.getOperation()) {
				case onClear:
					manager.clear(LEVEL_1, command.getRegion());
					break;
				case onDelete:
					manager.evict(LEVEL_1, command.getRegion(), command.getKey());
					break;
				default:
					break;
				}
			}
		}catch (Exception e) {
			error(e.getMessage(),e);
		}
	}
	@Override
	public void notifyCacheExpired(String region, Object key) {

		debug("Cache data expired, region=" + region + ",key=" + key);
		try{
		if (key instanceof List)
			manager.batchEvict(LEVEL_2, region, (List) key);
		else
			manager.evict(LEVEL_2, region, key);
		}catch (Exception e) {
			error(e.getMessage(),e);
		}


	}

}
