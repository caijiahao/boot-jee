package com.gitee.hermer.boot.jee.io.activemq;


import org.apache.activemq.ActiveMQConnectionFactory;
import org.apache.activemq.pool.PooledConnectionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import com.gitee.hermer.boot.jee.commons.dict.BootProperties;
import com.gitee.hermer.boot.jee.commons.exception.PaiUException;
import com.gitee.hermer.boot.jee.commons.log.UtilsContext;


@Configuration
public class ActiveMQConfiguration extends UtilsContext{

	@Autowired
	private BootProperties properties;

	@Bean(destroyMethod="stop")
	public  PooledConnectionFactory getConnectionFactory(){
		PooledConnectionFactory connectionFactoryBean = null;
		try {
			String brokerURL = properties.getDictValueString("wms.mq.url");
			String userName = properties.getDictValueString("wms.mq.username");
			String passWord = properties.getDictValueString("wms.mq.password");
			ActiveMQConnectionFactory connectionFactory = new ActiveMQConnectionFactory(userName, passWord, brokerURL);
			connectionFactoryBean = new PooledConnectionFactory();	
			connectionFactoryBean.setConnectionFactory(connectionFactory);
			connectionFactoryBean.setMaxConnections(2000);
		} catch (PaiUException e) {
			error(e.getMessage(),e);
		}
		return connectionFactoryBean;
	}
	
	@Override
	protected String getLogName() {
		// TODO Auto-generated method stub
		return "ActiveMQConfiguration";
	}


}
