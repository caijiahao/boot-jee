package com.gitee.hermer.boot.jee.metrice.service;

import java.util.Map;

/**
 * 
 * @ClassName: IHealthIndicator
 * @Description: 系统指标接口
 * @author:  涂孟超
 * @date: 2017年10月6日 下午5:31:50
 */
public interface IHealthIndicator {
	/**
	 * 
	 * @Title: 
	 * @Description: 获取指标信息
	 * @param: @return       
	 * @author:  涂孟超
	 * @date: 2017年10月6日 下午5:32:08
	 * @throws
	 */
	Map<String, String> metrice();
	

}
