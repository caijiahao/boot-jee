package com.gitee.hermer.boot.jee.ngrok;

import com.gitee.hermer.boot.jee.commons.log.UtilsContext;
import com.gitee.hermer.boot.jee.ngrok.woker.MessageListenerWorker;

import javax.net.ssl.SSLSocket;
import java.util.ArrayList;
import java.util.List;

/**
 * 启动客户端
 */
public class NgrokClient extends UtilsContext{

    private SSLSocket socket;
    private final SocketFactory socketFactory;
    private List<Tunnel> tunnels = new ArrayList<>();

    public NgrokClient(String serverAddress, int serverPort) {
        this.socketFactory = new SocketFactory(serverAddress, serverPort);
    }

    public void start() {
        try {
            this.socket = this.socketFactory.build();
        } catch (NgrokClientException e) {
            error("Ngrok Start failed: ", e);
            return;
        }

        MessageHandler messageHandler = new MessageHandler(socket, socketFactory, this.tunnels);
        WorkerPool.submit(new MessageListenerWorker(messageHandler));
        messageHandler.sendAuth();

    }

    public List<Tunnel> getTunnels() {
        return this.tunnels;
    }

    public NgrokClient addTunnel(Tunnel tunnel) {
        this.tunnels.add(tunnel);
        return this;
    }
}
