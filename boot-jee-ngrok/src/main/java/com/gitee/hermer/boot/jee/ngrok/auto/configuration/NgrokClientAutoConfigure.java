package com.gitee.hermer.boot.jee.ngrok.auto.configuration;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.annotation.Order;

import com.gitee.hermer.boot.jee.ngrok.properties.NgrokProperties;

@Configuration
@ConditionalOnProperty(prefix = "com.boot.jee.ngrok", name = "enable", havingValue = "true", matchIfMissing = false)
@EnableConfigurationProperties(value=NgrokProperties.class)
@Order(99)
public class NgrokClientAutoConfigure {

	
	@Autowired
	private NgrokProperties ngrokProperties;
	
    @Bean
    @ConditionalOnMissingBean(NgrokEmbeddedServletContainerInitializedEventListener.class)
    public NgrokEmbeddedServletContainerInitializedEventListener embeddedServletContainerInitializedEventListener() {
        return new NgrokEmbeddedServletContainerInitializedEventListener(ngrokProperties);
    }
}
