package com.gitee.hermer.boot.jee.orm;

import java.io.Serializable;
import java.util.List;


public interface IBaseDao<T,ID extends Serializable> {
	List<T> list(T t) throws Exception;
	Integer insert(T t) throws Exception;
	T find(T t) throws Exception;
	Integer update(T t) throws Exception;
	Integer delete(T t) throws Exception;
	Integer insertBatch(List<T> list) throws Exception;
	Integer updateBatch(List<T> list) throws Exception;
	Integer deleteBatch(List<T> list) throws Exception;
}

