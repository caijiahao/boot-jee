package com.gitee.hermer.boot.jee.upms.shiro.users.dao;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;
import com.gitee.hermer.boot.jee.orm.IBaseDao;
import com.gitee.hermer.boot.jee.upms.shiro.users.domain.SystemResource;

@Mapper
public interface ISystemResourceDao extends IBaseDao<SystemResource,Integer>{
	
	public List<SystemResource> listAccountResources(SystemResource t);
}
