package com.gitee.hermer.boot.jee.upms.shiro.users.service.impl;

import org.springframework.stereotype.Service;
import com.gitee.hermer.boot.jee.service.impl.BaseServiceImpl;
import com.gitee.hermer.boot.jee.upms.shiro.users.domain.AccountUser;
import com.gitee.hermer.boot.jee.upms.shiro.users.dao.IAccountUserDao;


@Service
public class AccountUserServiceImpl extends BaseServiceImpl<AccountUser, Integer, IAccountUserDao>{

}